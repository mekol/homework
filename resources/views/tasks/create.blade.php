@extends('layouts/app')

@section('title')
<title>Neue Aufgabe hinzufügen</title>
@section('content')
<style>
    .uper {
        margin-top: 40px;
    }
</style>

@if(Auth::user())
<div class="card uper">

    <div class="card-header">
        Neue Aufgabe hinzufügen
    </div>
    <div class="card-body">



        <form method="post" action="{{ route('tasks.store') }}">
            <div class="form-group">
                @csrf
                <label for="title">Aufgabe:</label>
                <input type="text" class="form-control" name="title" value="{{ old('title')}}">
                @if ($errors->has('title'))
                <span class="text-danger">{{ $errors->first('title') }}</span>
                @endif
            </div>
            <div class="form-group">
                <label for="description">Beschreibung:</label>
                <textarea class="form-control" name="description" rows="5" cols="50">{{old('description')}}</textarea>
                @if ($errors->has('description'))
                <span class="text-danger">{{ $errors->first('description') }}</span>
                @endif
            </div>

            <button type="submit" class="btn btn-primary">Hinzufügen</button>

            <a class="btn btn-primary" href="{{ route('tasks.index') }}"> Zurück</a>

        </form>
    </div>
</div>
@else
<div class="card-body">
    <h2>Fehler: Benutzer ist nicht angemeldet!</h2>
</div>
@endif
@endsection