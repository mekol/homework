@extends('layouts/app')

@section('title')
<title>Alle Aufgaben</title>
@section('content')
<style>
    .uper {
        margin-top: 40px;
    }
</style>
<div class="uper">
    @if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div><br />
    @endif


    <table class="table table-striped">
        <thead>
            <tr>
                <td>@sortablelink('id', 'ID')</td>
                <td>@sortablelink('title', 'Aufgabe')</td>
                <td>@sortablelink('created_at', 'Hinzugefügt am')</td>
                <td>@sortablelink('created_by','Hinzugefügt von')</td>
                <td>@sortablelink('completed','Status')</td>
                <td>Funktionen</td>
                @if(Auth::user())
                <td>Status ändern</td>
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach($tasks as $task)
            <tr>
                <td>{{$task->id}}</td>
                <td>{{$task->title}}</td>
                <td>{{$task->created_at}}</td>
                <td>{{$task->createdBy->name}}</td>
                <td>{{$task->completed == false ? 'Offen' : 'Abgeschlossen'}}</td>

                <td class="d-flex">
                    @if(Auth::user())
                    <a href="{{ route('tasks.edit', $task->id)}}"><img src="img/tools.png"></img></a>
                    @endif
                    <a href="{{ route('tasks.show', $task->id)}}" class="px-2"><img src="img/view.png"></img></a>
                    @if(Auth::user())
                    <form action="{{ route('tasks.destroy', $task->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <input type="image" src="img/bin.png" name="submit">
                    </form>
                    @endif
                </td>
                @if(Auth::user()) <td>
                    <form action="{{ route('tasks.completed', $task->id)}}" method="post">
                        @csrf
                        @method('PATCH')
                        <button class="btn btn-primary" type="submit">{{$task->completed == true ? 'Als offen markieren' : 'Als abgeschlossen markieren'}}</button>
                    </form>
                </td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
    <div>
        @endsection