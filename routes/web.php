<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/tasks');
});
Route::get('/tasks', 'TaskController@index');
Route::patch("tasks/{task}/completed", "TaskController@completed")->name('tasks.completed');

Route::resource("tasks", "TaskController");
Auth::routes();
