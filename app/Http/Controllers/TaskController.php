<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Task;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return response(Task::all()->jsonSerialize(), Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required|max:350'
        ]);
        $task = new Task();
        $task->title = $request->title;
        $task->description = $request->description;
        $task->created_by = Auth::id();
        $task->save();

        return response($task->jsonSerialize(), Response::HTTP_CREATED);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::findOrFail($id);

        return view('tasks/show', compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::findOrFail($id);

        return view('tasks/edit', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required|max:350'
        ]);
        $task->title = $request->title;
        $task->description = $request->description;
        $task->updated_by = Auth::id();
        $task->save();

        return response(null, Response::HTTP_OK);
    }

    /**
     * Sets a task to completed or in progress.
     *
     * @param Task $task
     * @return void
     */
    public function completed(Task $task)
    {
        if ($task->completed == false) {
            $task->completed = true;
            $task->update(["completed" => $task->completed]);
            return redirect('/tasks')->with('success', 'Aufgabe wurde abgeschlossen!');
        } else {
            $task->completed = false;
            $task->update(["completed" => $task->completed]);
            return redirect('/tasks')->with('success', 'Aufgabe wurde als offen markiert!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();

        return response(null, Response::HTTP_OK);
    }
}
