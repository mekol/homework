<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Kyslik\ColumnSortable\Sortable;

class Task extends Model
{
    use Sortable;

    protected $fillable = ["title", "description", "status"];
    public $sortable = ['id', 'title', 'created_at', 'created_by', 'completed'];

    /**
     * Creates a relationship between tasks and user.
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Creates a relationship between tasks and user.
     *
     * @return void
     */
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * Creates a relationship between tasks and user.
     *
     * @return void
     */
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
